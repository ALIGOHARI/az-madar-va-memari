
module adder64bit(
		      input [63:0] x,
                      input [63:0] y,
                      input inter,
                      output [63:0] e,
                      output sum
);

wire [3:0] a;

wire [3:0] b;

wire [4:0] c;



	fulladder16bit adder16bit0(x[15:0] ,  y[15:0] , inter , e[15:0] , c[1] ,  a[0] , a[0]);

	fulladder16bit adder16bit1(x[16:31] , y[16:31] , c[1] , e[16:31] , c[2] , a[1] , a[1]);

	fulladder16bit adder16bit2(x[32:47] , y[32:47] , c[2] , e[32:47] , c[3] , a[2] , a[2]);

	fulladder16bit adder16bit3(x[63:48] , y[63:48] , c[3] , e[63:48] , c[4] , a[1] , a[1]);

ad cl_adder_0(inter, a, b ,get1, get2,sum );
endmodule
