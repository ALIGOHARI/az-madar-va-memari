`timescale 1ns / 1ns
`include "../clock/clockgenerator.v"
`include "instructionmemory.v"

module instructionmemory_tb;
    wire clk;
    reg [5:0] address;
    wire [31:0] instruction;
    integer i;

    clockgenerator clock (clk);
    instructionmemory instructionmemory_instance (address, instruction);

    initial begin
        $dumpfile("instructionmemory_tb.vcd");
        $dumpvars(0, instructionmemory_tb);

        i = 0;
        while(i <= 15) begin
            @(posedge clk); address <= i;
            i = i+1;
        end

        #10 $finish;
    end

endmodule