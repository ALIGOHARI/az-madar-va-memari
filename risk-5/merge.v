//merge adder and shift rigester


`include "../adder/adder4bit.v"

`include "../register/register.v"

module merge
 (
    T,
    cl,
    res,
    e,
    sr, 
    sl
    )
    
    input [3:0]T;

    input [1:0]e;

    input cl;

    input res;

    input sr;

    input sl;

    wire [3:0]A;
    
    universal_register(A, T, cl, res, e, sr, sl)

    adder4bit(T , 4'b0100 , 1'b0 , A) 
    
endmodule
