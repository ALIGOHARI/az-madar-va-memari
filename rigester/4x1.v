module 4x1 (
	    a,
            b,
            c,
            d,
            e1,
            e2,
            );
    input wire a, b, c, d;
    input wire e1 , e2;
    output reg out;
    always @(a or b or c or d or e1 or e2)
    begin
        case ({e1 , e2})

            2'b00 : out <= a;
            2'b01 : out <= b;
            2'b10 : out <= c;
            2'b11 : out <= d;

        endcase
    end
    
endmodule
