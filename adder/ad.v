module ad(input inter,
           input [3:0] a,

           input [3:0] b,

           output sum,
           
	   output get1,
          
 	   output get2, );
          
    
    assign get1    = a[0]&a[1]&a[2]&a[3];

    assign get2    = b[3] | b[2]&a[3] | b[1]&a[3]&p[2] | b[0]&a[3]&a[2]&a[1];

    assign sum =(get1 &inter)+get1 ;

endmodule
