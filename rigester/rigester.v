module register(
   			A,
                        T,
                        cl,
                        res,
                        e,
                        sr,  
                        sl
); 
    wire [3:0] w;

    input [3:0]T;
    input [1:0]e ;
    input cl;
    input res;
    input sr;
    input sl;

    output [3:0]A;
    
    // mux

    	 4x1 mux1(T[0],sl,A[1],A[0],e[0],e[1],w[0]);

   	 4x1 mux2(T[1],A[0],A[2],A[1],e[0],e[1],w[1]);

   	 4x1 mux3(T[2],A[1],A[3],A[2],e[0],e[1], w[2]);

  	 4x1 mux4(T[3],A[2],sr,A[3],e[0],e[1],w[3]);
    
    //floop

    flop d1(w[0],A[0],cl,res);

    flop d2(w[1],A[1],cl,res);

    flop d3(w[2],A[2],cl,res);

    flop d4(w[3],A[3],cl,res);
    
endmodule


