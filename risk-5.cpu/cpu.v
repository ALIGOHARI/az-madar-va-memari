`include "Instruct/instructionmemory.v"
`include "data/datamemory.v"
`include "control/control.v"
`include "alu/alu_control.v"
`include "register/register_file.v"
`include "alu/alu.v"


module cpu(clk, address);
    input clk;
    input [5:0] address;
    wire [1:0] aluOp;
    wire [3:0] aluControl;
    wire [31:0] instruction;
    wire [63:0] writeData, readData, readData1, readData2,  secondInputAlu, aluResult;

    mux #(64) mux_instance1 (readData2, immOut, aluSrc, secondInputAlu);
    mux #(64) mux_instance2 (readData, aluResult, memToReg, writeData);

    instructionmemory instructionmemory_instance (address, instruction);
    datamemory datamemory_instance (clk, aluResult[5:0], readData2,  readData);

    control control_instance (instruction[6:0], branch, memRead, aluOp, memWrite, aluSrc, regWrite);
    alu_control alu_control_instance (aluOp, instruction[31:25], instruction[14:12], aluControl);

    register_file register_file_instance (regWrite, instruction[19:15], instruction[24:20], instruction[11:7], writeData, readData1, readData2, clk);

   

    alu alu_instance (readData1, secondInputAlu, aluControl, aluResult, zero);
endmodule