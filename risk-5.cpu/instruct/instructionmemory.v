module instructionmemory(address, instruction);
    input [5:0] address;
    output [31:0] instruction;
    reg [31:0] RAM[0:63];

    // some random data for initialization of RAM
    initial $readmemh("instructionmemory_tb.dat", RAM);

    assign instruction = RAM[address];
endmodule