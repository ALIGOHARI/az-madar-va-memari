`timescale 1ns / 1ns
`include "flipflop.v"
`include "../clock/clockgenerator.v"

module flipflop_tb;
    parameter n = 20;
    reg D;
    wire clk, Q;
    integer i;

    clockgenerator clock (clk);
    flipflop dff(D, clk, Q);

    initial begin
        $dumpfile("flipflop_tb.vcd");
        $dumpvars(0, flipflop_tb);

        D <= 0;

        for(i = 0; i < n; i=i+1) begin
            D = i[1];
            display;
        end
    end

    task display;
      #10 $display("Clock = ", clk, " D = ", D, " Q = ", Q);
    endtask
endmodule 