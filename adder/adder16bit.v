
module adder4bit(input [15:0] x,
                      input [15:0] y,
                      input inter,
                      output [15:0] e,
                      output sum,
                      output get1,
                      output get2
);

wire [3:0] a;
wire [3:0] b;
wire [4:0] c;


//full adder

	fulladder4bit adder4bit0(x[3:0] ,  y[3:0] ,  inter , e[3:0] , c[1] , a[0] , b[0]);
	
	fulladder4bit adder4bit1(x[7:4] ,  y[7:4] ,  c[1] , e[7:4] , c[2] , a[1] , b[1]);
	
	fulladder4bit adder4bit2(x[11:8] , y[11:8] , c[2] , e[11:8] , c[3] , a[2] , b[2]);
	
	fulladder4bit adder4bit3(x[15:12] ,y[15:12] ,c[3] , e[7:4] , c[4] , a[1] , b[1]);

ad cl_adder_0(inter ,a , b, get1,get2 , sum);

endmodule
