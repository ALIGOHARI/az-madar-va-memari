module mer ;



    reg[3:0] T;

    reg clk; reg res;

    reg [1:0] e;

    reg sr;
 
    reg sl; 

    wire [3:0]A; 

   
 project_number_3 usr(.T(T), .cl(cl), .res(res), .e(e), .sr(sr),
 .sl(sl));


initial begin
    
    // shift right 

    T     = 4'b0000;
    cl   = 1'b1;
    res = 1'b1;
    sr = 1'b1;
    sl = 1'b0;
    e = 2'b01;
    
    // shift left test

    #40;
    res = 1'b1;
    sr = 1'b0;
    sl = 1'b1;
    e = 2'b10;
    
  

    #40;
    sl = 1'b0;
    rest = 1'b1;
    T = 4'b0101;
    e = 2'b11;
    
end

always #10 cl = ~cl;

end
