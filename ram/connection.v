`include "./ram.v"
`include "../risk-5/merge.v"

module ram-start;
    
    reg cl, res,e[1:0], input, sr, sl, reg regdata;

    reg Data;
 
    output wire output;
    
   	 rik-5 merge (.cl(cl) , .res(res),.e(e),.input(.input),.sr(sr),.sl(sl),.output(output));
   	 ram ram(.Data(Data), .address(output) , .output(regdata));
    
    initial begin
        Data[0]  = $random%10;

        Data[4]  = $random%10;

        Data[8]  = $random%10;

        Data[12] = $random%10;

        res  = 0;

        e       = b'11;

        $display ("reg: %d"    , output);

        #10 $display ("reg: %d", output);

        #10 $display ("reg: %d", output);

        #10 $display ("reg: %d", output);
    end
    
    
    always #10 cl = ~cl;
    
    
endmodule
