
module flop(
		   D,
                   Q,
                   cl,
                   res
);
    input D;

    input cl;

    input res;

    output reg Q;

    always @(negedge cl)
      begin

        if (res == 1'b1)
            Q <= 1'b0;
        else
            Q <= D;

    end
endmodule
