module ram (
	    Data,
            address,
            output,
            );
    
    parameter addressSZ = 64;

    parameter dataSZ  = 32;
    
    input [addressSZ-1:0] address;

    output [dataSZ-1:0] output ;
    
    input reg [addressSZ-1:0] Data [dataSZ-1 : 0]; 
    
    always @(*)

    begin

        output = Data[address] ;

    end

    
endmodule
