
module fulladder(
		  input x,
                  input y,
                  input inter,

                  output e,
                  );
    
    assign e = x ^ y ^ inter;                

endmodule