
module cla_adder_4bit(input [3:0] x,
                      input [3:0] y,
                      input inter,
                      output [3:0] e,
                      output sum,
                      output get1,
                      output get2
);

wire [3:0] a = x ^ y; 
wire [3:0] b = x & y;
wire [3:0] c ;



assign c[0]  = inter;

assign c[1]  = b[0] | a[0]&c[0];

assign c[2]  = b[1] | b[0]&a[1] | c[0]&a[0]&a[1];

assign c[3]  = b[2] | b[1]&a[2] | b[0]&a[1]&a[2] | c[0]&a[0]&a[1]&a[2];

assign c_out = b[3] | b[2]&a[3] | b[1]&a[2]&a[3] | b[0]&a[1]&a[2]&a[3]|
c[0]&a[0]&a[1]&a[2]&a[3];
//full adder
	fulladder fa0(x[0] , y[0] , c[0] , e[0]);
	fulladder fa1(x[1] , y[1] , c[1] , e[1]);
	fulladder fa2(x[2] , y[2] , c[2] , e[2]);
	fulladder fa3(x[3] , y[3] , c[3] , e[3]);

ad cl_adder_0(inter, a, b ,get1, get2,sum ;

endmodule
